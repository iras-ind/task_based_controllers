#ifndef TASK_JOINT_SSM_CONTROLLER____
#define TASK_JOINT_SSM_CONTROLLER____

#include <actionlib/server/action_server.h>
#include <thor_prefilter_controller/thor_prefilter_controller.h>
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <itia_basic_hardware_interface/posveleff_command_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <controller_interface/controller.h>
#include <subscription_notifier/subscription_notifier.h>
#include <sensor_msgs/JointState.h>
#include <eigen_conversions/eigen_msg.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <geometry_msgs/Pose.h>
#include <task_math/task_math.h>
#include <task_math/common_tasks.h>
#include <task_math/common_limits.h>
#include <task_based_controller/task_based_controller.h>
#include <urdf_parser/urdf_parser.h>


namespace taskQP
{
    
  class TaskJointSSMController: public taskQP::TaskBasedController
  {
  public:
    TaskJointSSMController();
    ~TaskJointSSMController();

    virtual bool init(ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
    virtual bool update(const ros::Time& time, const ros::Duration& period);
    virtual void starting(const ros::Time& time, const std::vector<double>& qini, const std::vector<double>& Dqini);
    virtual void stopping(const ros::Time& time);
    void distanceCallback(const std_msgs::Float64ConstPtr& msg);

  protected:
    urdf::Model m_model;
    rosdyn::ChainPtr m_chain_ee;
    rosdyn::ChainPtr m_chain_elb;
    double m_distance_from_obstacle;
    double m_slowdown_distance;
    double m_stop_distance;
    std::vector<double> m_target_scaling_poly_coeff;  

    std::shared_ptr<ros_helper::SubscriptionNotifier<std_msgs::Float64>> m_distance_notif;
    ros::Publisher m_pub_distance_in_motion;
    ros::Publisher m_y_elbow_pub;


  };

  class TaskJointSSMPosVelEffController : public thor::PrefilterPosVelEffController
  {
  public:
    virtual bool init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
  };

  class TaskJointSSMPosController : public thor::PrefilterPosController{
  public:
   virtual bool init(hardware_interface::PositionJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
  };
};

#endif
