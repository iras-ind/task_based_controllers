#ifndef TASK_CARTESIAN_CONTROLLER____
#define TASK_CARTESIAN_CONTROLLER____

#include <actionlib/server/action_server.h>
#include <thor_prefilter_controller/thor_prefilter_controller.h>
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <itia_basic_hardware_interface/posveleff_command_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <controller_interface/controller.h>
#include <subscription_notifier/subscription_notifier.h>
#include <sensor_msgs/JointState.h>
#include <eigen_conversions/eigen_msg.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <task_math/task_math.h>
#include <task_math/common_tasks.h>
#include <task_math/common_limits.h>
#include <urdf_parser/urdf_parser.h>


namespace taskQP
{
    
  class TaskCartesianController: public thor::PrefilterController
  {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    TaskCartesianController();
    ~TaskCartesianController();

    virtual bool init(ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
    virtual bool update(const ros::Time& time, const ros::Duration& period);
    virtual void starting(const ros::Time& time, const std::vector<double>& qini, const std::vector<double>& Dqini);
    virtual void stopping(const ros::Time& time);

  protected:

    double m_out_of_path_scaling;
    trajectory_msgs::JointTrajectoryPoint m_prefilter_pnt;

    PrefilterController m_prefilter;
    unsigned int m_np;
    math::virtualModelPtr m_mpc_model;
    math::TaskStack m_sot;
    math::LimitsArray m_ineq_array;

    urdf::Model m_model;
    rosdyn::ChainPtr m_chain_ee;

    Eigen::VectorXd m_target_Dx;
    Eigen::Affine3d m_target_X_now;
    Eigen::VectorXd m_target_Q;
    unsigned int m_task_size;

    Eigen::VectorXd m_target_Dq;
    double m_target_scaling;
    Eigen::VectorXd m_target_Q_now;
    Eigen::VectorXd m_next_acc;
    double m_scaling;
    Eigen::VectorXd m_sol;

    ros::Publisher m_tolerance_pub;
    ros::Publisher m_scaling_pub;
    ros::Publisher m_q_nominal_pub;
    ros::Publisher m_y_elbow_pub;
    ros::Publisher m_dq_ratio_pub;

    double t_max;
    double t_mean;
    int iter;

    bool m_use_smooth_swap;
    std::vector<double> m_smooth_coeff;
    Eigen::VectorXd m_delta_q_ref;
    Eigen::VectorXd m_delta_q_max;
    double m_delta_q_act;
    double m_delta_q_old;
    double m_delta_q_int;

    bool m_use_delta_q_max;
    bool m_use_delta_q_min;

  };

  class TaskCartesianPosVelEffController : public thor::PrefilterPosVelEffController
  {
  public:
    virtual bool init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
  };

  class TaskCartesianPosController : public thor::PrefilterPosController
  {
  public:
    virtual bool init(hardware_interface::PositionJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
  };
};

#endif
