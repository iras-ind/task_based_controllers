#ifndef TASK_BASED_CONTROLLER____
#define TASK_BASED_CONTROLLER____

#include <actionlib/server/action_server.h>
#include <thor_prefilter_controller/thor_prefilter_controller.h>
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <itia_basic_hardware_interface/posveleff_command_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <controller_interface/controller.h>
#include <subscription_notifier/subscription_notifier.h>
#include <sensor_msgs/JointState.h>
#include <eigen_conversions/eigen_msg.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <task_math/task_math.h>
#include <task_math/common_tasks.h>
#include <task_math/common_limits.h>

namespace taskQP
{
    
  class TaskBasedController: public thor::PrefilterController
  {
  public:
    TaskBasedController();
    ~TaskBasedController();

    virtual bool init(ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
    virtual bool update(const ros::Time& time, const ros::Duration& period);
    virtual void starting(const ros::Time& time, const std::vector<double>& qini, const std::vector<double>& Dqini);
    virtual void stopping(const ros::Time& time);

  protected:

    double m_out_of_path_scaling;
    trajectory_msgs::JointTrajectoryPoint m_prefilter_pnt;

    PrefilterController m_prefilter;
    unsigned int m_np;
    math::virtualModelPtr m_mpc_model;
    math::TaskStack m_sot;
    math::LimitsArray m_ineq_array;

    Eigen::VectorXd m_target_Dq;
    double m_target_scaling;
    Eigen::VectorXd m_target_Q_now;
    Eigen::VectorXd m_next_acc;
    double m_scaling;
    Eigen::VectorXd m_sol;

    ros::Publisher m_tolerance_pub;
    ros::Publisher m_scaling_pub;

  };

  class TaskBasedPosVelEffController : public thor::PrefilterPosVelEffController
  {
  public:
    virtual bool init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
  };

  class TaskBasedPosController : public thor::PrefilterPosController
  {
  public:
    virtual bool init(hardware_interface::PositionJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
  };
};

#endif
