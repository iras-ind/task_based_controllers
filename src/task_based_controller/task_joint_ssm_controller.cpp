#include <task_based_controller/task_joint_ssm_controller.h>
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(taskQP::TaskJointSSMPosController, controller_interface::ControllerBase);
PLUGINLIB_EXPORT_CLASS(taskQP::TaskJointSSMPosVelEffController, controller_interface::ControllerBase);

namespace taskQP
{

  TaskJointSSMController::TaskJointSSMController( )
  {
  }

  TaskJointSSMController::~TaskJointSSMController( )
  {
  }

  bool TaskJointSSMController::init(ros::NodeHandle &root_nh, ros::NodeHandle &controller_nh)
  {
    ROS_INFO("initializing %s",controller_nh.getNamespace().c_str());

    ROS_INFO("%s: initializing TaskBasedController",controller_nh.getNamespace().c_str());
    taskQP::TaskBasedController::init(root_nh,controller_nh);
    ROS_INFO("%s: initialized TaskBasedController",controller_nh.getNamespace().c_str());

    /* Init chain */
    std::string base_frame;
    if (!m_controller_nh.getParam("base_frame", base_frame))
    {
      ROS_ERROR("base_frame not defined");
      return false;
    }
    std::string tool_frame;
    if (!m_controller_nh.getParam("tool_frame", tool_frame))
    {
      ROS_ERROR("tool_frame not defined");
      return false;
    }
    if (!m_controller_nh.getParam("controlled_joint",m_joint_names))
    {
      ROS_ERROR("controlled_joint not defined");
      return false;
    }
    std::string robot_description;
    if (!m_nh.getParam("/robot_description", robot_description))
    {
      ROS_FATAL_STREAM("Parameter '/robot_description' does not exist");
      return false;
    }
    m_model.initParam("robot_description");
    Eigen::Vector3d grav;
    grav << 0, 0, -9.806;
    m_chain_ee = rosdyn::createChain(m_model,base_frame,tool_frame,grav);
    m_chain_ee->setInputJointsName(m_joint_names);

    // TEST FOR RAL PAPER
    m_chain_elb = rosdyn::createChain(m_model,base_frame,"ur5_forearm_link",grav);
    ROS_INFO("%s: Create chain",controller_nh.getNamespace().c_str());

    /* Get robot limits from urdf */
    urdf::ModelInterfaceSharedPtr model_inteface;
    model_inteface = urdf::parseURDF(robot_description);
    std::vector<double> q_max(m_nax,0.0);
    std::vector<double> q_min(m_nax,0.0);
    std::vector<double> dq_max(m_nax,3.0);
    std::vector<double> dq_min(m_nax,-3.0);
    std::vector<double> ddq_max(m_nax,4.0);
    std::vector<double> ddq_min(m_nax,-4.0);
    for (unsigned int iAx=0; iAx<m_nax; iAx++)
    {
      ROS_INFO("%s: joint %s",controller_nh.getNamespace().c_str(),m_joint_names.at(iAx).c_str());
      q_max.at(iAx) = model_inteface->getJoint(m_joint_names.at(iAx))->limits->upper;
      q_min.at(iAx) = model_inteface->getJoint(m_joint_names.at(iAx))->limits->lower;
      if ((q_max.at(iAx)==0) && (q_min.at(iAx)==0))
      {
        q_max.at(iAx)=std::numeric_limits<double>::infinity();
        q_min.at(iAx)=-std::numeric_limits<double>::infinity();
        ROS_INFO("upper and lower limits are both equal to 0, set +/- infinity");
      }
      bool has_velocity_limits;
      if (!m_root_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/has_velocity_limits",has_velocity_limits))
        has_velocity_limits=false;
      bool has_acceleration_limits;
      if (!m_root_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/has_acceleration_limits",has_acceleration_limits))
        has_acceleration_limits=false;

      dq_max.at(iAx)= model_inteface->getJoint(m_joint_names.at(iAx))->limits->velocity;
      if (has_velocity_limits)
      {
        double vel;
        if (!m_root_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/max_velocity",vel))
        {
          ROS_ERROR_STREAM("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/max_velocity is not defined");
          return false;
        }
        if (vel<dq_max.at(iAx))
          dq_max.at(iAx)=vel;
          dq_min.at(iAx)=-vel;
      }

      if (has_acceleration_limits)
      {
        double acc;
        if (!m_root_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/max_acceleration",acc))
        {
          ROS_ERROR_STREAM("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/max_acceleration is not defined");
          return false;
        }
        ddq_max.at(iAx)=acc;
        ddq_min.at(iAx)=-acc;
      }
      else
        ddq_max.at(iAx)=10*ddq_max.at(iAx);
    }

    ROS_INFO("%s: create limits",controller_nh.getNamespace().c_str());
    math::UpperAccelerationLimitsPtr acc_ub_ptr = std::static_pointer_cast<math::UpperAccelerationLimits>(m_ineq_array.getConstraint(0));
    acc_ub_ptr->setLimits(ddq_max);
    math::LowerAccelerationLimitsPtr acc_lb_ptr = std::static_pointer_cast<math::LowerAccelerationLimits>(m_ineq_array.getConstraint(1));
    acc_lb_ptr->setLimits(ddq_min);
    math::UpperVelocityLimitsPtr vel_ub_ptr = std::static_pointer_cast<math::UpperVelocityLimits>(m_ineq_array.getConstraint(2));
    vel_ub_ptr->setLimits(dq_max);
    math::LowerVelocityLimitsPtr vel_lb_ptr = std::static_pointer_cast<math::LowerVelocityLimits>(m_ineq_array.getConstraint(3));
    vel_lb_ptr->setLimits(dq_min);
    math::UpperPositionLimitsPtr pos_ub_ptr = std::static_pointer_cast<math::UpperPositionLimits>(m_ineq_array.getConstraint(4));
    pos_ub_ptr->setLimits(q_max);
    math::LowerPositionLimitsPtr pos_lb_ptr = std::static_pointer_cast<math::LowerPositionLimits>(m_ineq_array.getConstraint(5));
    pos_lb_ptr->setLimits(q_min);
    math::UpperInvarianceConstraintPtr inv_ub_ptr = std::static_pointer_cast<math::UpperInvarianceConstraint>(m_ineq_array.getConstraint(6));
    inv_ub_ptr->setLimits(q_max,dq_max,ddq_min);
    math::LowerInvarianceConstraintPtr inv_lb_ptr = std::static_pointer_cast<math::LowerInvarianceConstraint>(m_ineq_array.getConstraint(7));
    inv_lb_ptr->setLimits(q_min,dq_min,ddq_max);

    m_slowdown_distance=1.2;
    m_stop_distance=0.2;
    m_distance_from_obstacle=1.0e3;

    m_target_scaling_poly_coeff.resize(4);
    m_target_scaling_poly_coeff.at(0)=-2/pow(m_slowdown_distance - m_stop_distance,3);
    m_target_scaling_poly_coeff.at(1)=(3*(m_slowdown_distance + m_stop_distance))/pow(m_slowdown_distance - m_stop_distance,3);
    m_target_scaling_poly_coeff.at(2)=-(6*m_slowdown_distance*m_stop_distance)/pow(m_slowdown_distance - m_stop_distance,3);
    m_target_scaling_poly_coeff.at(3)=(pow(m_stop_distance,2)*(3*m_slowdown_distance - m_stop_distance))/((m_slowdown_distance - m_stop_distance)*(pow(m_slowdown_distance,2) - 2*m_slowdown_distance*m_stop_distance + pow(m_stop_distance,2)));


    ROS_INFO("%s: Create topics",controller_nh.getNamespace().c_str());
    /* Subscribe to obstacle topics */
    m_distance_notif.reset(new ros_helper::SubscriptionNotifier<std_msgs::Float64>(m_controller_nh,"/distance",1));
    m_distance_notif->setAdvancedCallback(boost::bind(&TaskJointSSMController::distanceCallback,this,_1));

    /* Publisher distance when robot is moving (for binary logger) */
    m_pub_distance_in_motion = m_nh.advertise<std_msgs::Float64>("/distance_in_motion",1);

    // TEST RAL PAPER
    m_y_elbow_pub   = m_nh.advertise<std_msgs::Float64>("/y_elbow",1);


    ROS_INFO("%s: Controller initialized",controller_nh.getNamespace().c_str());

    return true;

  }

  void TaskJointSSMController::starting(const ros::Time& time, const std::vector< double >& qini, const std::vector< double >& Dqini)
  {
    ROS_INFO("%s: Controller starting",m_controller_nh.getNamespace().c_str());
    taskQP::TaskBasedController::starting(time,qini,Dqini);

  }

  void TaskJointSSMController::stopping(const ros::Time& time)
  {
    thor::PrefilterController::stopping(time);
  }

  bool TaskJointSSMController::update(const ros::Time& time, const ros::Duration& period)
  {

    m_queue.callAvailable();

    m_mtx.lock();

    /* Computed desired pose at t0 */
    m_thor_prefilter->interpolate(m_scaled_time,m_prefilter_pnt,1);
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
      m_target_Q_now(iAx)=m_prefilter_pnt.positions.at(iAx);

    /* Compute desired trajectory*/
    for (unsigned int iP=0;iP<m_mpc_model->get_np();iP++)
    {
      m_thor_prefilter->interpolate(m_scaled_time+ros::Duration(m_mpc_model->getPredictionTimes()(iP)),m_prefilter_pnt,m_global_override);
      for (unsigned int iAx=0;iAx<m_nax;iAx++)
        m_target_Dq(iAx+iP*m_nax)=m_prefilter_pnt.velocities.at(iAx);
    }
//    if (!m_is_in_path_tolerance)
//      m_out_of_path_scaling=std::max(0.1,m_out_of_path_scaling*0.999);
//    else
//      m_out_of_path_scaling=0.99*m_out_of_path_scaling+0.01;

    /* Modify scaling reference based on distance from closest obstacle */
    double target_scaling_ssm=1.0;
    if (m_distance_from_obstacle>m_slowdown_distance)
      target_scaling_ssm=1.0;
    else if (m_distance_from_obstacle<m_stop_distance)
      target_scaling_ssm=1.0e-9;
    else
      target_scaling_ssm=m_target_scaling_poly_coeff.at(0)*pow(m_distance_from_obstacle,3)+m_target_scaling_poly_coeff.at(1)*pow(m_distance_from_obstacle,2)+m_target_scaling_poly_coeff.at(2)*m_distance_from_obstacle+m_target_scaling_poly_coeff.at(3);

    m_target_scaling=target_scaling_ssm*m_global_override*m_out_of_path_scaling;

    /* Set reference trajectory*/
    math::JointVelocityTaskPtr vel_task_ptr = std::static_pointer_cast<math::JointVelocityTask>(m_sot.getTask(0));
    vel_task_ptr->setTargetScaling(m_target_scaling);
    vel_task_ptr->setTargetTrajectory(m_target_Dq,m_target_Q_now);

    /* Update tasks */
    for (size_t idx=0; idx<m_sot.stackSize(); idx++)
      m_sot.getTask(idx)->update( m_mpc_model->getPositionPrediction(),m_mpc_model->getVelocityPrediction() );
    //NOTA: il get delle predizioni potrebbe essere fatto in automatico nel task dato che ha accesso al model

//          taskQP::math::JointVelocityTask* t = ((taskQP::math::JointVelocityTask*)m_sot.getTask(i));
//          t->setTargetScaling(m_target_scaling);
//          t->update(m_target_Dq,m_mpc_model.getState().head(m_nax),m_mpc_model.getState().tail(m_nax));

    for (size_t idx=0; idx<m_ineq_array.arraySize(); idx++)
      m_ineq_array.getConstraint(idx)->update( m_mpc_model->getPositionPrediction(),m_mpc_model->getVelocityPrediction() );

    Eigen::MatrixXd CE;  // null matrix
    Eigen::VectorXd ce0; // null vector
    if (m_distance_from_obstacle<=0.8*m_stop_distance)
    {
      ROS_INFO_THROTTLE(0.5,"human closer than stop threshold");
      m_next_acc = -m_mpc_model->getState().tail(m_nax)/m_mpc_model->getSamplingPeriod();
      m_scaling = 0.0;
    }
    else
    {
      math::computeHQPSolution(m_sot,CE,ce0,m_ineq_array.matrix(),m_ineq_array.vector(),m_sol);
//      math::computeHQPSolution(m_sot,CE,ce0,CE,ce0,m_sol);
      m_next_acc =m_sol.head(m_nax);
      m_scaling=m_sol(m_nax*m_np);
    }

    ROS_WARN_THROTTLE(2,"s_ref = %f \t s=%f ",m_target_scaling,m_scaling);

    m_mpc_model->updatePredictions(m_sol.head(m_nax*m_np)); // updatePredictions sempre prima di updateState
    m_mpc_model->updateState(m_next_acc);
    m_scaled_time+=period*m_scaling;
    m_time+=period;

    m_is_in_tolerance=true;
    m_is_in_path_tolerance=true;
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
    {
      m_pnt.positions.at(iAx)=m_mpc_model->getState()(iAx);
      m_pnt.velocities.at(iAx)=m_mpc_model->getState()(iAx+m_nax);
      m_pnt.accelerations.at(iAx)=m_next_acc(iAx);
      if ( std::abs(m_pnt.positions.at(iAx)-m_target_Q_now(iAx) ) >m_goal_tolerance)
        m_is_in_tolerance=false;
      if ( std::abs(m_pnt.positions.at(iAx)-m_target_Q_now(iAx) ) >m_path_tolerance)
        m_is_in_path_tolerance=false;
    }

    m_is_in_tolerance=m_is_in_tolerance && (m_scaled_time-m_thor_prefilter->trjTime()).toSec()>0;
    m_mtx.unlock();

    std_msgs::BoolPtr msg(new std_msgs::Bool());;
    msg->data=m_is_in_tolerance && m_is_in_path_tolerance;
    m_tolerance_pub.publish(msg);

    std_msgs::Float64Ptr scaling_msg(new std_msgs::Float64());
    scaling_msg->data=m_scaling;
    m_scaling_pub.publish(scaling_msg);

    std_msgs::Float64Ptr distance_motion_msg(new std_msgs::Float64());
    distance_motion_msg->data=m_distance_from_obstacle;
    if (m_mpc_model->getState().segment(m_nax,m_nax).norm()>1.0e-2)
      m_pub_distance_in_motion.publish(distance_motion_msg);

    // TEST For RAL paper
    Eigen::Affine3d Tw_elb=m_chain_elb->getTransformation(m_mpc_model->getState().head(4));
    std_msgs::Float64Ptr y_elbow_msg(new std_msgs::Float64());
    y_elbow_msg->data=Tw_elb.translation()(1); // here
    m_y_elbow_pub.publish(y_elbow_msg);



//    ROS_WARN_STREAM("pos = " << m_mpc_model->getState().segment(0,m_nax).transpose());
//    ROS_INFO_STREAM("vel = " << m_mpc_model->getState().segment(m_nax,m_nax).transpose());
//    ROS_ERROR_STREAM("vel_target = " << m_target_Dq.head(m_nax).transpose());
//    ROS_ERROR_STREAM("acc = " << m_next_acc.transpose());
//    ROS_ERROR("s_ref=%f \t scaling=%f\n",m_target_scaling,m_scaling);

    return true;
  }

  void TaskJointSSMController::distanceCallback(const std_msgs::Float64ConstPtr& msg)
  {
    m_distance_from_obstacle=msg->data;
  }

  bool TaskJointSSMPosVelEffController::init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
  {
    m_ctrl.reset(new TaskJointSSMController());
    m_root_nh = root_nh;
    m_controller_nh = controller_nh;
    m_hw = hw;
    if (!m_ctrl->init(root_nh,controller_nh))
      return false;

    m_joint_names=m_ctrl->getJointNames();
    m_nAx=m_joint_names.size();
    for (unsigned int iDim = 0;iDim<m_nAx;iDim++)
    {
      try
      {
        m_handles.push_back(m_hw->getHandle(m_joint_names.at(iDim)));
      }
      catch (std::exception& ex)
      {
        ROS_ERROR("%s is not handled by %s. expection: %s",m_joint_names.at(iDim).c_str(),root_nh.getNamespace().c_str(),ex.what());
        return false;
      }
    }
    return true;
  }

  bool TaskJointSSMPosController::init(hardware_interface::PositionJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
  {
    m_ctrl.reset(new TaskJointSSMController());
    m_root_nh = root_nh;
    m_controller_nh = controller_nh;
    m_hw = hw;
    if (!m_ctrl->init(root_nh,controller_nh))
      return false;

    m_joint_names=m_ctrl->getJointNames();
    m_nAx=m_joint_names.size();

    for (unsigned int iDim = 0;iDim<m_nAx;iDim++)
    {
      try
      {
        m_handles.push_back(m_hw->getHandle(m_joint_names.at(iDim)));
      }
      catch (std::exception& ex)
      {
        ROS_ERROR("%s is not handled by %s. expection: %s",m_joint_names.at(iDim).c_str(),root_nh.getNamespace().c_str(),ex.what());
        return false;
      }
    }

    return true;
  }

}
