#include <task_based_controller/task_cartesian_controller.h>
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(taskQP::TaskCartesianPosController, controller_interface::ControllerBase);
PLUGINLIB_EXPORT_CLASS(taskQP::TaskCartesianPosVelEffController, controller_interface::ControllerBase);

namespace taskQP
{

  TaskCartesianController::TaskCartesianController( )
  {
  }

  TaskCartesianController::~TaskCartesianController( )
  {
  }

  bool TaskCartesianController::init(ros::NodeHandle &root_nh, ros::NodeHandle &controller_nh)
  {
    /* Init Prefilter */
    thor::PrefilterController::init(root_nh,controller_nh);
    m_out_of_path_scaling=1;
    m_tolerance_pub = m_controller_nh.advertise<std_msgs::Bool>("in_tolerance",1);
    m_scaling_pub   = m_controller_nh.advertise<std_msgs::Float64>("scaling",1);
    m_goal_tolerance=0.0001;

    m_q_nominal_pub = m_controller_nh.advertise<sensor_msgs::JointState>("/joint_nominal_trj",1);
    m_y_elbow_pub   = m_controller_nh.advertise<std_msgs::Float64>("/y_elbow",1);
    m_dq_ratio_pub  = m_controller_nh.advertise<std_msgs::Float64>("/dq_ratio",1);



    /* Get param for MPC initialization */
    m_np=5;
    double control_horizon=1;
    double st=0.008;
    if (!m_controller_nh.getParam("control_horizon",control_horizon))
      ROS_ERROR("unable to load param control_horizon. default: %f", control_horizon);
    if (!m_controller_nh.getParam("period",st))
      ROS_ERROR("unable to load param period. default: %f", st);
    int tmp=m_np;
    if (!m_controller_nh.getParam("prediction_size",tmp))
    {
      ROS_ERROR("unable to load param prediction_size. default: %d", m_np);
    }
    m_np=tmp;

    /* Get param */
    double clik_gain=1;
    if (!m_controller_nh.getParam("clik_gain",clik_gain))
      ROS_ERROR("unable to load param clik_gain. default: %f", clik_gain);
    std::vector<int> selection_axis{1, 1, 1, 1, 1, 1};
    if (!m_controller_nh.getParam("selection_axis",selection_axis))
      ROS_ERROR("unable to load param selection_axis. deafult: all axis activated");
    std::string base_frame;
    if (!m_controller_nh.getParam("base_frame", base_frame))
    {
      ROS_ERROR("base_frame not defined");
      return false;
    }
    std::string tool_frame;
    if (!m_controller_nh.getParam("tool_frame", tool_frame))
    {
      ROS_ERROR("tool_frame not defined");
      return false;
    }
    std::string elbow_frame;
    if (!m_controller_nh.getParam("elbow_frame", elbow_frame))
    {
      ROS_ERROR("elbow_frame not defined");
      return false;
    }
    if (!m_controller_nh.getParam("controlled_joint",m_joint_names))
    {
      ROS_ERROR("controlled_joint not defined");
      return false;
    }

    /* Init chain */
    std::string robot_description;
    if (!m_nh.getParam("/robot_description", robot_description))
    {
      ROS_FATAL_STREAM("Parameter '/robot_description' does not exist");
      return false;
    }
    m_model.initParam("robot_description");
    Eigen::Vector3d grav;
    grav << 0, 0, -9.806;
    m_chain_ee = rosdyn::createChain(m_model,base_frame,tool_frame,grav);
    m_chain_ee->setInputJointsName(m_joint_names);

    /* Init elbow chain */
    rosdyn::ChainPtr chain_elbow;
    chain_elbow = rosdyn::createChain(m_model,base_frame,elbow_frame,grav);

    /* Initialize MPC, SoT, limits array */
    bool set_joint_trj_as_task=true;
    if (!m_controller_nh.getParam("set_joint_trj_as_task",set_joint_trj_as_task))
      ROS_ERROR("unable to load param set_joint_trj_as_task. default: true");

    m_mpc_model.reset(new math::virtualModel());
    m_mpc_model->setPredictiveHorizon(control_horizon);
    m_mpc_model->init(m_nax,m_np,st);
    m_sot.set_n_axis(m_nax);
    m_sot.set_np(m_np);
    m_ineq_array.set_n_axis(m_nax);
    m_ineq_array.set_np(m_np);

    /* Create tasks ptr */
    math::MinimizeAccelerationPtr minimize_acc_ptr(new taskQP::math::MinimizeAcceleration(m_mpc_model));
    math::MinimizeVelocityPtr mininimize_vel_ptr(new taskQP::math::MinimizeVelocity(m_mpc_model));
    math::CartesianTaskPtr cartesian_task_ptr(new taskQP::math::CartesianTask(m_mpc_model,m_chain_ee,selection_axis));
    math::JointPositionTaskPtr joint_task_ptr(new taskQP::math::JointPositionTask(m_mpc_model));
    std::vector<int> selection_axis_elbow{0, 1, 0, 0, 0, 0};
    math::CartesianTaskPtr elbow_task_ptr(new taskQP::math::CartesianTask(m_mpc_model,chain_elbow,selection_axis_elbow));

    /* Activate trajectory scaling */
    cartesian_task_ptr->activateScaling(true);
    cartesian_task_ptr->setWeightScaling(1.0e-3);
    cartesian_task_ptr->enableClik(true);
    cartesian_task_ptr->setWeightClik(clik_gain);
    m_task_size=cartesian_task_ptr->getTaskSize();

    /* Fill SoT and cast to BaseConstraint */
    m_sot.taskPushBack( (math::BaseConstraintPtr) cartesian_task_ptr,1.0);
    m_sot.taskPushBack( (math::BaseConstraintPtr) elbow_task_ptr,1.0e-3);
    m_sot.taskPushBack( (math::BaseConstraintPtr) joint_task_ptr,((double) set_joint_trj_as_task)*1.0e-4);
    m_sot.taskPushBack( (math::BaseConstraintPtr) mininimize_vel_ptr,1.0e-6);
    m_sot.taskPushBack( (math::BaseConstraintPtr) minimize_acc_ptr,1.0e-9 );

    /* Get robot limits from urdf */
    urdf::ModelInterfaceSharedPtr model_inteface;
    model_inteface = urdf::parseURDF(robot_description);
    std::vector<double> q_max(m_nax,0.0);
    std::vector<double> q_min(m_nax,0.0);
    std::vector<double> dq_max(m_nax,3.0);
    std::vector<double> dq_min(m_nax,-3.0);
    std::vector<double> ddq_max(m_nax,4.0);
    std::vector<double> ddq_min(m_nax,-4.0);
    for (unsigned int iAx=0; iAx<m_nax; iAx++)
    {
      q_max.at(iAx) = model_inteface->getJoint(m_joint_names.at(iAx))->limits->upper;
      q_min.at(iAx) = model_inteface->getJoint(m_joint_names.at(iAx))->limits->lower;
      if ((q_max.at(iAx)==0) && (q_min.at(iAx)==0))
      {
        q_max.at(iAx)=std::numeric_limits<double>::infinity();
        q_min.at(iAx)=-std::numeric_limits<double>::infinity();
        ROS_INFO("upper and lower limits are both equal to 0, set +/- infinity");
      }
      bool has_velocity_limits;
      if (!m_root_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/has_velocity_limits",has_velocity_limits))
        has_velocity_limits=false;
      bool has_acceleration_limits;
      if (!m_root_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/has_acceleration_limits",has_acceleration_limits))
        has_acceleration_limits=false;

      dq_max.at(iAx)= model_inteface->getJoint(m_joint_names.at(iAx))->limits->velocity;
      if (has_velocity_limits)
      {
        double vel;
        if (!m_root_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/max_velocity",vel))
        {
          ROS_ERROR_STREAM("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/max_velocity is not defined");
          return false;
        }
        if (vel<dq_max.at(iAx))
          dq_max.at(iAx)=vel;
          dq_min.at(iAx)=-vel;
      }

      if (has_acceleration_limits)
      {
        double acc;
        if (!m_root_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/max_acceleration",acc))
        {
          ROS_ERROR_STREAM("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/max_acceleration is not defined");
          return false;
        }
        ddq_max.at(iAx)=acc;
        ddq_min.at(iAx)=-acc;
      }
      else
        ddq_max.at(iAx)=10*ddq_max.at(iAx);
    }

    if (!m_controller_nh.getParam("use_delta_q_max", m_use_delta_q_max))
    {
      ROS_ERROR("use_delta_q_max not defined");
      return false;
    }
    if (!m_controller_nh.getParam("use_delta_q_min", m_use_delta_q_min))
    {
      ROS_ERROR("use_delta_q_min not defined");
      return false;
    }

    std::vector<double> delta_max(m_nax);
    for (unsigned int idx=0;idx<m_nax;idx++)
         delta_max.at(idx)=0.3;
    if (!m_controller_nh.getParam("delta_q_max",delta_max))
      ROS_ERROR("unable to load param delta_q_max. deafult: %f for all axis",delta_max.at(0));
    m_delta_q_max.resize(m_nax);
    for (unsigned int idx=0;idx<m_nax;idx++)
         m_delta_q_max(idx)=delta_max.at(idx);

    for (unsigned int idx=0;idx<m_nax;idx++)
      ROS_ERROR("delta q_max=%f", m_delta_q_max(idx));

    std::vector<double> temp(m_nax);
    for (unsigned int idx=0;idx<m_nax;idx++)
         temp.at(idx)=0.3;
    if (!m_controller_nh.getParam("delta_q_ref",temp))
      ROS_ERROR("unable to load param delta_q_ref. deafult: %f for all axis",temp.at(0));
    m_delta_q_ref.resize(m_nax);
    for (unsigned int idx=0;idx<m_nax;idx++)
         m_delta_q_ref(idx)=temp.at(idx);

    for (unsigned int idx=0;idx<m_nax;idx++)
      ROS_WARN("delta q_ref=%f", m_delta_q_ref(idx));

    math::UpperAccelerationLimitsPtr ub_acc(new math::UpperAccelerationLimits(m_mpc_model,ddq_max));
    math::LowerAccelerationLimitsPtr lb_acc(new math::LowerAccelerationLimits(m_mpc_model,ddq_min));
    math::UpperVelocityLimitsPtr ub_vel(new math::UpperVelocityLimits(m_mpc_model,dq_max));
    math::LowerVelocityLimitsPtr lb_vel(new math::LowerVelocityLimits(m_mpc_model,dq_min));
    math::UpperPositionLimitsPtr ub_pos(new math::UpperPositionLimits(m_mpc_model,q_max));
    math::UpperInvarianceConstraintPtr ub_invariance(new math::UpperInvarianceConstraint(m_mpc_model,q_max,dq_max,ddq_min));
    math::LowerPositionLimitsPtr lb_pos(new math::LowerPositionLimits(m_mpc_model,q_min));
    math::LowerInvarianceConstraintPtr lb_invariance(new math::LowerInvarianceConstraint(m_mpc_model,q_min,dq_min,ddq_max));
    math::UpperPositionVariationPtr ub_delta(new math::UpperPositionVariation(m_mpc_model,delta_max));
    math::UpperInvarianceVariationPtr ub_delta_inv(new math::UpperInvarianceVariation(m_mpc_model,delta_max,dq_max,ddq_min));
    math::LowerPositionVariationPtr lb_delta(new math::LowerPositionVariation(m_mpc_model,delta_max));
    math::LowerInvarianceVariationPtr lb_delta_inv(new math::LowerInvarianceVariation(m_mpc_model,delta_max,dq_min,ddq_max));
    math::ScalingLimitsPtr max_scaling(new math::ScalingLimits(m_mpc_model,1.5));

    /* Fill LimitsArray */
    m_ineq_array.addConstraint(ub_acc);
    m_ineq_array.addConstraint(lb_acc);
    m_ineq_array.addConstraint(ub_vel);
    m_ineq_array.addConstraint(lb_vel);
    if (!m_use_delta_q_max)
    {
      m_ineq_array.addConstraint(ub_pos);
      m_ineq_array.addConstraint(ub_invariance);
    }
    else
    {
      m_ineq_array.addConstraint(ub_delta);
      m_ineq_array.addConstraint(ub_delta_inv);
    }
    if (!m_use_delta_q_min)
    {
      m_ineq_array.addConstraint(lb_pos);
      m_ineq_array.addConstraint(lb_invariance);
    }
    else
    {
      m_ineq_array.addConstraint(lb_delta);
      m_ineq_array.addConstraint(lb_delta_inv);
    }
    m_ineq_array.addConstraint(max_scaling);

    /* Resize signals */
    m_target_Dq.resize(m_nax*m_np);
    m_target_Q_now.resize(m_nax);
    m_target_Dx.resize(m_np*6);
    m_target_Q.resize(m_nax*m_np);
    m_next_acc.resize(m_nax);
    m_sol.resize(m_np*m_nax+m_np);
    m_scaling=1.0;

    /* Swap tasks for limited Dq*/
    m_use_smooth_swap=false;
    if (!m_controller_nh.getParam("use_smooth_swap",m_use_smooth_swap))
      ROS_ERROR("unable to load param use_smooth_swap. default: false");

//    m_delta_q_max=0.15;
//    m_delta_q_act=0.10;
//    m_smooth_coeff.resize(4);
//    m_smooth_coeff.at(0)=-2/pow(m_delta_q_act - m_delta_q_max,3);
//    m_smooth_coeff.at(1)=(3*(m_delta_q_act + m_delta_q_max))/pow(m_delta_q_act - m_delta_q_max,3);
//    m_smooth_coeff.at(2)=-(6*m_delta_q_act*m_delta_q_max)/pow(m_delta_q_act - m_delta_q_max,3);
//    m_smooth_coeff.at(3)=(pow(m_delta_q_max,2)*(3*m_delta_q_act - m_delta_q_max))/((m_delta_q_act - m_delta_q_max)*(pow(m_delta_q_act,2) - 2*m_delta_q_act*m_delta_q_max + pow(m_delta_q_max,2)));

    m_delta_q_old=0.0;
    m_delta_q_int=0.0;

    ROS_INFO("Controller initialized");

    return true;
  }

  void TaskCartesianController::starting(const ros::Time& time, const std::vector< double >& qini, const std::vector< double >& Dqini)
  {
    /* Starting prefilter and virtual model with current state */
    thor::PrefilterController::starting(time,qini,Dqini);

    Eigen::VectorXd initial_state(2*m_nax);
    for (unsigned int i=0;i<qini.size();i++)
    {
      initial_state(i)=qini.at(i);
      initial_state(i+qini.size())=Dqini.at(i);
    }

    m_prefilter_pnt.positions   = qini;
    m_prefilter_pnt.velocities  = Dqini;
    m_prefilter_pnt.accelerations.resize(m_nax,0);
    m_prefilter_pnt.effort.resize(m_nax,0);
    m_prefilter_pnt.time_from_start=ros::Duration(0);

    m_mpc_model->setInitialState( initial_state );
    m_target_Dq.setZero();
    m_target_Q_now=m_mpc_model->getState().head(m_nax);
    m_target_Dx.setZero();
    m_target_X_now=m_chain_ee->getTransformation(m_mpc_model->getState().head(m_nax));
    for (unsigned int iNp=0;iNp<m_np;iNp++)
      m_target_Q.segment(iNp*m_nax,m_nax)=m_target_Q_now;
    m_next_acc.setZero();
    m_sol.setZero();

    t_mean=0;
    t_max=0;
    iter=0;

    ROS_INFO("Controller started");

  }

  void TaskCartesianController::stopping(const ros::Time& time)
  {
    thor::PrefilterController::stopping(time);
  }

  bool TaskCartesianController::update(const ros::Time& time, const ros::Duration& period)
  {
    m_queue.callAvailable();

    ros::Time t_start=ros::Time::now();

    m_mtx.lock();

    /* Computed desired pose at t0 */
    m_thor_prefilter->interpolate(m_scaled_time,m_prefilter_pnt,1);
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
      m_target_Q_now(iAx)=m_prefilter_pnt.positions.at(iAx);
    m_target_X_now=m_chain_ee->getTransformation(m_target_Q_now);

    /* Compute desired trajectory*/
    for (unsigned int iP=0;iP<m_mpc_model->get_np();iP++)
    {
      m_thor_prefilter->interpolate(m_scaled_time+ros::Duration(m_mpc_model->getPredictionTimes()(iP)),m_prefilter_pnt,m_global_override);
      for (unsigned int iAx=0;iAx<m_nax;iAx++)
      {
        m_target_Dq(iAx+iP*m_nax)=m_prefilter_pnt.velocities.at(iAx);
        m_target_Q(iAx+iP*m_nax)=m_prefilter_pnt.positions.at(iAx);
      }
      m_target_Dx.segment(iP*6,6)=m_chain_ee->getTwistTool(m_target_Q.segment(iP*m_nax,m_nax),m_target_Dq.segment(iP*m_nax,m_nax));
    }

//    if (!m_is_in_path_tolerance)
//      m_out_of_path_scaling=std::max(0.1,m_out_of_path_scaling*0.999);
//    else
//      m_out_of_path_scaling=0.99*m_out_of_path_scaling+0.01;
    m_target_scaling=m_global_override*m_out_of_path_scaling;

    ros::Time t_trj=ros::Time::now();
//    ROS_INFO("iter=%d, trj = \t %f",iter,(t_trj-t_start).toSec());

    /* Set reference trajectory*/
    math::CartesianTaskPtr cartesian_task_ptr = std::static_pointer_cast<math::CartesianTask>(m_sot.getTask(0));
    cartesian_task_ptr->setTargetScaling(m_target_scaling);
    cartesian_task_ptr->setTargetTrajectory(m_target_Dx,m_target_X_now);

    math::CartesianTaskPtr elbow_task_ptr = std::static_pointer_cast<math::CartesianTask>(m_sot.getTask(1));
    Eigen::VectorXd elbow_vel(6*m_np);
    elbow_vel.setZero();
    for (unsigned int iNp=0;iNp<m_np;iNp++)
      elbow_vel(1+6*iNp)=0.5*(-1.0-elbow_task_ptr->getTransformation().translation()(1));
    elbow_task_ptr->setTargetTrajectory(elbow_vel,m_target_X_now);
//    ROS_INFO_STREAM_ONCE("elbow_task_ptr" << elbow_vel.transpose());

    math::JointPositionTaskPtr joint_task_ptr = std::static_pointer_cast<math::JointPositionTask>(m_sot.getTask(2));
    joint_task_ptr->setTargetPosition(m_target_Q);

    if (m_use_delta_q_max)
    {
      math::UpperPositionVariationPtr ub_delta = std::static_pointer_cast<math::UpperPositionVariation>( m_ineq_array.getConstraint(4));
      ub_delta->setTargetTrajectory(m_target_Q_now,m_target_Q);
      math::UpperInvarianceVariationPtr ub_delta_inv = std::static_pointer_cast<math::UpperInvarianceVariation>( m_ineq_array.getConstraint(5));
      ub_delta_inv->setTargetTrajectory(m_target_Q_now,m_target_Q);
//      ROS_ERROR_STREAM_THROTTLE(1,"target q0" << m_target_Q_now.transpose());
//      ROS_ERROR_STREAM_THROTTLE(1,"target q_" << m_target_Q.transpose());

    }
    if (m_use_delta_q_min)
    {
       math::LowerPositionVariationPtr lb_delta = std::static_pointer_cast<math::LowerPositionVariation>( m_ineq_array.getConstraint(6));
       lb_delta->setTargetTrajectory(m_target_Q_now,m_target_Q);
       math::LowerInvarianceVariationPtr lb_delta_inv = std::static_pointer_cast<math::LowerInvarianceVariation>( m_ineq_array.getConstraint(7));
       lb_delta_inv->setTargetTrajectory(m_target_Q_now,m_target_Q);
    }

    ros::Time t_set=ros::Time::now();
//    ROS_WARN("iter=%d, set = \t %f",iter,(t_set-t_trj).toSec());

    /* Adapt second task lambda with PID to reach equilibrium delta_q->delta_q_max */
    double cl_weight0=1.0e-3;
    double cl_weight=cl_weight0;
    double delta_q_normalized = ( (m_target_Q_now-m_mpc_model->getState().head(m_nax)).cwiseQuotient(m_delta_q_ref).cwiseAbs() ).maxCoeff();
    double delta_dq= delta_q_normalized-m_delta_q_old;
    m_delta_q_int+=(1.0-delta_q_normalized)*0.008;
    m_delta_q_old=delta_q_normalized;

    if (m_use_smooth_swap)
    {
      cl_weight=cl_weight0*0.5*( (1.0-delta_q_normalized)+20*m_delta_q_int+20*(-delta_dq) );
      if (cl_weight<=1.0e-9) // antiwindup
      {
        cl_weight=1.0e-9;
        if (1.0-delta_q_normalized < 0)
          m_delta_q_int-=(1.0-delta_q_normalized)*0.008;
      }
      if (cl_weight>=2*cl_weight0)
      {
        cl_weight=cl_weight0;
        if (1.0-delta_q_normalized > 0)
          m_delta_q_int-=(1.0-delta_q_normalized)*0.008;
      }
    }
    else
      cl_weight=0.1*cl_weight0;

    m_sot.setWeight(cl_weight,1);

    /* Update tasks */
    for (size_t idx=0;idx<m_sot.stackSize();idx++)
      m_sot.getTask(idx)->update( m_mpc_model->getPositionPrediction(),m_mpc_model->getVelocityPrediction() );
      //NOTA: il get delle predizioni potrebbe essere fatto in automatico nel task dato che ha accesso al model

    ros::Time t_up1=ros::Time::now();
//    ROS_ERROR("iter=%d, up1 = \t %f",iter,(t_up1-t_set).toSec());

    for (size_t idx=0; idx<m_ineq_array.arraySize(); idx++)
    {
      m_ineq_array.getConstraint(idx)->update( m_mpc_model->getPositionPrediction(),m_mpc_model->getVelocityPrediction() );
    }

    ros::Time t_up2=ros::Time::now();
//    ROS_INFO("iter=%d, up2 = \t %f",iter,(t_up2-t_up1).toSec());
//    ROS_INFO_STREAM_THROTTLE(2,"b4=\n" << (m_ineq_array.getConstraint(4)->b()).transpose());
//    ROS_INFO_STREAM_THROTTLE(2,"b5=\n" << (m_ineq_array.getConstraint(5)->b()).transpose());
//    ROS_INFO_STREAM_THROTTLE(2,"b6=\n" << (m_ineq_array.getConstraint(6)->b()).transpose());
//    ROS_INFO_STREAM_THROTTLE(2,"b7=\n" << (m_ineq_array.getConstraint(7)->b()).transpose());

    Eigen::MatrixXd CE;  // null matrix
    Eigen::VectorXd ce0; // null vector
    math::computeHQPSolution(m_sot,CE,ce0,m_ineq_array.matrix(),m_ineq_array.vector(),m_sol);
    m_next_acc =m_sol.head(m_nax);
    m_scaling=m_sol(m_nax*m_np);

    if (std::abs(m_scaling)>2)
    {
      Eigen::VectorXd sol_unc;
      sol_unc.resize((m_nax+1)*m_np);
      double cost=math::computeHQPSolution(m_sot,CE,ce0,CE,ce0,sol_unc);

      ROS_WARN_STREAM("sol_unc=\n" << sol_unc.transpose());
      ROS_INFO("Cost=%f",cost);

      ROS_WARN_STREAM("CE=\n" << CE);
      ROS_WARN_STREAM("ce0=\n" << ce0.transpose());
      ROS_ERROR_STREAM("A4=\n" << m_ineq_array.getConstraint(4)->A());
      ROS_INFO_STREAM("b4=\n" << (m_ineq_array.getConstraint(4)->b()).transpose());
      ROS_WARN_STREAM("A5=\n" << m_ineq_array.getConstraint(5)->A());
      ROS_INFO_STREAM("b5=\n" << (m_ineq_array.getConstraint(5)->b()).transpose());
      ROS_ERROR_STREAM("A6=\n" << m_ineq_array.getConstraint(6)->A());
      ROS_INFO_STREAM("b6=\n" << (m_ineq_array.getConstraint(6)->b()).transpose());
      ROS_WARN_STREAM("A7=\n" << m_ineq_array.getConstraint(7)->A());
      ROS_INFO_STREAM("b7=\n" << (m_ineq_array.getConstraint(7)->b()).transpose());
      ROS_ERROR_STREAM("sol=\n" << m_sol.transpose());

    }

    ros::Time t_qps=ros::Time::now();
//    ROS_WARN("iter=%d, qps = \t %f",iter,(t_qps-t_up2).toSec());
//    ROS_INFO_STREAM("sol=" << m_sol.transpose() );
//    ROS_INFO("scaling=%f",m_scaling);
   // ROS_WARN_THROTTLE(0.5,"scaling = %f \t ll=%f \t delta_q = %f \t delta_q = %f",m_scaling, ll, (m_target_Q_now-m_mpc_model->getState().head(m_nax)).minCoeff(),(m_target_Q_now-m_mpc_model->getState().head(m_nax)).maxCoeff());

    double dq_ratio_ref = ( (m_target_Q_now-m_mpc_model->getState().head(m_nax)).cwiseQuotient(m_delta_q_ref).cwiseAbs() ).maxCoeff();
    double dq_ratio_max = ( (m_target_Q_now-m_mpc_model->getState().head(m_nax)).cwiseQuotient(m_delta_q_max).cwiseAbs() ).maxCoeff();
    ROS_WARN_THROTTLE(0.2,"s = %f \t w=%f \t dq/dqref=%f \t dq/dqmax=%f",m_scaling, cl_weight, dq_ratio_ref, dq_ratio_max);


    m_is_in_tolerance=true;
    m_is_in_path_tolerance=true;
    Eigen::VectorXd cartesian_error(cartesian_task_ptr->getTaskSize());
    cartesian_error=cartesian_task_ptr->computeTaskError( m_target_X_now, m_mpc_model->getState().head(m_nax) );
    if (cartesian_error.cwiseAbs().maxCoeff()>m_goal_tolerance)
      m_is_in_tolerance=false;
    if (cartesian_error.cwiseAbs().maxCoeff()>m_path_tolerance)
      m_is_in_path_tolerance=false;

    m_mpc_model->updatePredictions(m_sol.head(m_nax*m_np)); // updatePredictions always before updateState
    m_mpc_model->updateState(m_next_acc);
    m_scaled_time+=period*m_scaling;
    m_time+=period;

    for (unsigned int iAx=0;iAx<m_nax;iAx++)
    {
      m_pnt.positions.at(iAx)=m_mpc_model->getState()(iAx);
      m_pnt.velocities.at(iAx)=m_mpc_model->getState()(iAx+m_nax);
      m_pnt.accelerations.at(iAx)=m_next_acc(iAx);
    }

    m_is_in_tolerance=m_is_in_tolerance && (m_scaled_time-m_thor_prefilter->trjTime()).toSec()>0;
    m_mtx.unlock();

    std_msgs::BoolPtr msg(new std_msgs::Bool());;
    msg->data=m_is_in_tolerance && m_is_in_path_tolerance;
    m_tolerance_pub.publish(msg);

    std_msgs::Float64Ptr scaling_msg(new std_msgs::Float64());
    scaling_msg->data=m_scaling;
    m_scaling_pub.publish(scaling_msg);

    sensor_msgs::JointStatePtr qnom_msg(new sensor_msgs::JointState());
    qnom_msg->position.resize(m_nax);
    for (unsigned inax=0;inax<m_nax;inax++)
      qnom_msg->position.at(inax)=m_target_Q_now(inax);
    m_q_nominal_pub.publish(qnom_msg);


    Eigen::Affine3d Tw_elb=elbow_task_ptr->getDynamicsChain()->getTransformation(m_mpc_model->getState());
    std_msgs::Float64Ptr y_elbow_msg(new std_msgs::Float64());
    y_elbow_msg->data=Tw_elb.translation()(1); // here
    m_y_elbow_pub.publish(y_elbow_msg);

    std_msgs::Float64Ptr dq_ratio_msg(new std_msgs::Float64());
    dq_ratio_msg->data=dq_ratio_max; // here
    m_dq_ratio_pub.publish(dq_ratio_msg);






    ros::Time t_snd=ros::Time::now();
//    ROS_WARN("iter=%d, snd = \t %f",iter,(t_snd-t_qps).toSec());

    return true;
  }

  bool TaskCartesianPosVelEffController::init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
  {
    m_ctrl.reset(new TaskCartesianController());
    m_root_nh = root_nh;
    m_controller_nh = controller_nh;
    m_hw = hw;
    if (!m_ctrl->init(root_nh,controller_nh))
      return false;

    m_joint_names=m_ctrl->getJointNames();
    m_nAx=m_joint_names.size();
    for (unsigned int iDim = 0;iDim<m_nAx;iDim++)
    {
      try
      {
        m_handles.push_back(m_hw->getHandle(m_joint_names.at(iDim)));
      }
      catch (std::exception& ex)
      {
        ROS_ERROR("%s is not handled by %s. expection: %s",m_joint_names.at(iDim).c_str(),root_nh.getNamespace().c_str(),ex.what());
        return false;
      }
    }

    return true;
  }

  bool TaskCartesianPosController::init(hardware_interface::PositionJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
  {
    m_ctrl.reset(new TaskCartesianController());
    m_root_nh = root_nh;
    m_controller_nh = controller_nh;
    m_hw = hw;
    if (!m_ctrl->init(root_nh,controller_nh))
      return false;

    m_joint_names=m_ctrl->getJointNames();
    m_nAx=m_joint_names.size();
    for (unsigned int iDim = 0;iDim<m_nAx;iDim++)
    {
      try
      {
        m_handles.push_back(m_hw->getHandle(m_joint_names.at(iDim)));
      }
      catch (std::exception& ex)
      {
        ROS_ERROR("%s is not handled by %s. expection: %s",m_joint_names.at(iDim).c_str(),root_nh.getNamespace().c_str(),ex.what());
        return false;
      }
    }

    return true;
  }

}
