#include <task_based_controller/task_based_controller.h>
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(taskQP::TaskBasedPosController, controller_interface::ControllerBase);
PLUGINLIB_EXPORT_CLASS(taskQP::TaskBasedPosVelEffController, controller_interface::ControllerBase);

namespace taskQP
{

  TaskBasedController::TaskBasedController( )
  {
  }

  TaskBasedController::~TaskBasedController( )
  {
  }

  bool TaskBasedController::init(ros::NodeHandle &root_nh, ros::NodeHandle &controller_nh)
  {
    /* Init Prefilter */
    thor::PrefilterController::init(root_nh,controller_nh);
    m_out_of_path_scaling=1;
    m_tolerance_pub = m_controller_nh.advertise<std_msgs::Bool>("in_tolerance",1);
    m_scaling_pub   = m_controller_nh.advertise<std_msgs::Float64>("scaling",1);

    /* Get param for MPC initialization */
    m_np=5;
    double control_horizon=1;
    double st=0.008;
    if (!m_controller_nh.getParam("control_horizon",control_horizon))
      ROS_ERROR("unable to load param control_horizon. default: %f", control_horizon);
    if (!m_controller_nh.getParam("period",st))
      ROS_ERROR("unable to load param period. default: %f", st);
    int tmp=m_np;
    if (!m_controller_nh.getParam("prediction_size",tmp))
    {
      ROS_ERROR("unable to load param prediction_size. default: %d", m_np);
    }
    m_np=tmp;

    /* Get param for clik*/
    double clik_gain=1;
    if (!m_controller_nh.getParam("clik_gain",clik_gain))
      ROS_ERROR("unable to load param clik_gain. default: %f", clik_gain);


    ROS_DEBUG("[ %s ] creating MPC problem",m_controller_nh.getNamespace().c_str());
    /* Initialize MPC, SoT, limits array */
    m_mpc_model.reset(new math::virtualModel());
    m_mpc_model->setPredictiveHorizon(control_horizon);

    m_mpc_model->init(m_nax,m_np,st);
    m_sot.set_n_axis(m_nax);
    m_sot.set_np(m_np);
    m_ineq_array.set_n_axis(m_nax);
    m_ineq_array.set_np(m_np);

    /* Create tasks ptr */
//  taskQP::math::MinimizeAccelerationPtr minimize_acc(new taskQP::math::MinimizeAcceleration(m_nax,nc));
//  minimize_acc.reset(new taskQP::math::MinimizeAcceleration(m_nax,nc));
//  minimize_acc = std::make_shared<taskQP::math::MinimizeAcceleration>(m_nax,nc);

//    taskQP::math::MinimizeAcceleration* t = ((taskQP::math::MinimizeAcceleration*)m_stack.at(0));

//    delete m_stack.at (0);
//    m_stack.at (0) = nullptr;
    ROS_DEBUG("[ %s ] creating MPC cost function",m_controller_nh.getNamespace().c_str());

    math::MinimizeAccelerationPtr minimize_acc_ptr(new math::MinimizeAcceleration(m_mpc_model));
//    minimize_acc_ptr->initMPC(m_mpc_model);
    math::MinimizeVelocityPtr mininimize_vel_ptr(new math::MinimizeVelocity(m_mpc_model));
//    mininimize_vel_ptr->initMPC(m_mpc_model);
    math::JointVelocityTaskPtr vel_task_ptr(new math::JointVelocityTask(m_mpc_model));
//    vel_task_ptr->initMPC(m_mpc_model);

    /* Activate trajectory scaling */
    vel_task_ptr->activateScaling(true);
    vel_task_ptr->setWeightScaling(1.0e-4);
    vel_task_ptr->enableClik(true);
    vel_task_ptr->setWeightClik(clik_gain);
//    vel_task_ptr->init();

    ROS_DEBUG("[ %s ] creating MPC SoT",m_controller_nh.getNamespace().c_str());

    /* Fill SoT and cast to BaseConstraint */
    m_sot.taskPushBack( (math::BaseConstraintPtr) vel_task_ptr,1.0);
    m_sot.taskPushBack( (math::BaseConstraintPtr) mininimize_vel_ptr,1.0e-7);
    m_sot.taskPushBack( (math::BaseConstraintPtr) minimize_acc_ptr,1.0e-9 );

    /* Create limits ptr */
    std::vector<double> ddq_max{ 5, 5, 5, 10, 10, 10, 10 };
    std::vector<double> ddq_min{-5,-5,-5,-10,-10,-10,-10 };
    std::vector<double> dq_max{  1.0, 3.1, 3.1, 3.1, 3.1, 3.1, 3.1 };
    std::vector<double> dq_min{ -1.0,-3.1,-3.1,-3.1,-3.1,-3.1,-3.1 };
    std::vector<double> q_max{  1.70, 6.28, 6.28, 3.14, 6.28, 6.28, 6.28 };
    std::vector<double> q_min{  0.01,-6.28,-6.28,-3.14,-6.28,-6.28,-6.28 };
    ROS_DEBUG("[ %s ] creating MPC Limit",m_controller_nh.getNamespace().c_str());

    math::UpperAccelerationLimitsPtr ub_acc(new math::UpperAccelerationLimits(m_mpc_model,ddq_max));
    math::LowerAccelerationLimitsPtr lb_acc(new math::LowerAccelerationLimits(m_mpc_model,ddq_min));
    math::UpperVelocityLimitsPtr ub_vel(new math::UpperVelocityLimits(m_mpc_model,dq_max));
    math::LowerVelocityLimitsPtr lb_vel(new math::LowerVelocityLimits(m_mpc_model,dq_min));
    math::UpperPositionLimitsPtr ub_pos(new math::UpperPositionLimits(m_mpc_model,q_max));
    math::LowerPositionLimitsPtr lb_pos(new math::LowerPositionLimits(m_mpc_model,q_min));
    math::UpperInvarianceConstraintPtr ub_invariance(new math::UpperInvarianceConstraint(m_mpc_model,q_max,dq_max,ddq_min));
    math::LowerInvarianceConstraintPtr lb_invariance(new math::LowerInvarianceConstraint(m_mpc_model,q_min,dq_min,ddq_max));
    math::ScalingLimitsPtr max_scaling(new math::ScalingLimits(m_mpc_model,1.5));

    /* Fill LimitsArray */
    m_ineq_array.addConstraint(ub_acc);
    m_ineq_array.addConstraint(lb_acc);
    m_ineq_array.addConstraint(ub_vel);
    m_ineq_array.addConstraint(lb_vel);
    m_ineq_array.addConstraint(ub_pos);
    m_ineq_array.addConstraint(lb_pos);
    m_ineq_array.addConstraint(ub_invariance);
    m_ineq_array.addConstraint(lb_invariance);
    m_ineq_array.addConstraint(max_scaling);

    /* Resize signals */
    m_target_Dq.resize(m_np*m_nax);
    m_target_Q_now.resize(m_nax);
    m_next_acc.resize(m_nax);
    m_sol.resize(m_np*m_nax+m_np);

    ROS_DEBUG("[ %s ] Successfully Initialization of TTaskBasedController",m_controller_nh.getNamespace().c_str());

    return true;
  }

  void TaskBasedController::starting(const ros::Time& time, const std::vector< double >& qini, const std::vector< double >& Dqini)
  {
    /* Starting prefilter and virtual model with current state */
    thor::PrefilterController::starting(time,qini,Dqini);

    Eigen::VectorXd initial_state(2*m_nax);
    for (unsigned int i=0;i<qini.size();i++)
    {
      initial_state(i)=qini.at(i);
      initial_state(i+qini.size())=Dqini.at(i);
    }

    m_prefilter_pnt.positions   = qini;
    m_prefilter_pnt.velocities  = Dqini;
    m_prefilter_pnt.accelerations.resize(m_nax,0);
    m_prefilter_pnt.effort.resize(m_nax,0);
    m_prefilter_pnt.time_from_start=ros::Duration(0);

    m_mpc_model->setInitialState( initial_state );
    m_target_Dq.setZero();
    m_target_Q_now=m_mpc_model->getState().head(m_nax);
    m_next_acc.setZero();
    m_scaling=1.0;
    m_sol.setZero();

    ROS_INFO("[ %s ] Controller started",m_controller_nh.getNamespace().c_str());

  }

  void TaskBasedController::stopping(const ros::Time& time)
  {
    thor::PrefilterController::stopping(time);
  }

  bool TaskBasedController::update(const ros::Time& time, const ros::Duration& period)
  {


    m_queue.callAvailable();

    m_mtx.lock();

    /* Computed desired pose at t0 */
    m_thor_prefilter->interpolate(m_scaled_time,m_prefilter_pnt,1);
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
      m_target_Q_now(iAx)=m_prefilter_pnt.positions.at(iAx);

    /* Compute desired trajectory*/
    for (unsigned int iP=0;iP<m_mpc_model->get_np();iP++)
    {
      m_thor_prefilter->interpolate(m_scaled_time+ros::Duration(m_mpc_model->getPredictionTimes()(iP)),m_prefilter_pnt,m_global_override);
      for (unsigned int iAx=0;iAx<m_nax;iAx++)
        m_target_Dq(iAx+iP*m_nax)=m_prefilter_pnt.velocities.at(iAx);
    }
//    if (!m_is_in_path_tolerance)
//      m_out_of_path_scaling=std::max(0.1,m_out_of_path_scaling*0.999);
//    else
//      m_out_of_path_scaling=0.99*m_out_of_path_scaling+0.01;
    m_target_scaling=m_global_override*m_out_of_path_scaling;
    // m_thorQP.computedCostrainedSolution(m_target_Dq,m_next_Q,m_target_scaling,m_thorQP.getState(),m_next_acc,m_scaling);

    /* Set reference trajectory*/
    math::JointVelocityTaskPtr vel_task_ptr = std::static_pointer_cast<math::JointVelocityTask>(m_sot.getTask(0));
    vel_task_ptr->setTargetScaling(m_target_scaling);
    vel_task_ptr->setTargetTrajectory(m_target_Dq,m_target_Q_now);

    /* Update tasks */
    for (size_t idx=0; idx<m_sot.stackSize(); idx++)
      m_sot.getTask(idx)->update( m_mpc_model->getPositionPrediction(),m_mpc_model->getVelocityPrediction() );
    //NOTA: il get delle predizioni potrebbe essere fatto in automatico nel task dato che ha accesso al model

//          taskQP::math::JointVelocityTask* t = ((taskQP::math::JointVelocityTask*)m_sot.getTask(i));
//          t->setTargetScaling(m_target_scaling);
//          t->update(m_target_Dq,m_mpc_model.getState().head(m_nax),m_mpc_model.getState().tail(m_nax));

    for (size_t idx=0; idx<m_ineq_array.arraySize(); idx++)
      m_ineq_array.getConstraint(idx)->update( m_mpc_model->getPositionPrediction(),m_mpc_model->getVelocityPrediction() );

    Eigen::MatrixXd CE;  // null matrix
    Eigen::VectorXd ce0; // null vector
    math::computeHQPSolution(m_sot,CE,ce0,m_ineq_array.matrix(),m_ineq_array.vector(),m_sol);
    m_next_acc =m_sol.head(m_nax);
    m_scaling=m_sol(m_nax*m_np);
//    if (m_next_acc.maxCoeff()>0.01)
//      ROS_INFO_STREAM("sol = " << m_next_acc.transpose() << " " << m_scaling);

    ROS_WARN_THROTTLE(2,"s_ref = %f \t s=%f ",m_target_scaling,m_scaling);


    m_mpc_model->updatePredictions(m_sol.head(m_nax*m_np)); // updatePredictions sempre prima di updateState
    m_mpc_model->updateState(m_next_acc);
    m_scaled_time+=period*m_scaling;
    m_time+=period;

    m_is_in_tolerance=true;
    m_is_in_path_tolerance=true;
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
    {
      m_pnt.positions.at(iAx)=m_mpc_model->getState()(iAx);
      m_pnt.velocities.at(iAx)=m_mpc_model->getState()(iAx+m_nax);
      m_pnt.accelerations.at(iAx)=m_next_acc(iAx);
      if ( std::abs(m_pnt.positions.at(iAx)-m_target_Q_now(iAx) ) >m_goal_tolerance)
        m_is_in_tolerance=false;
      if ( std::abs(m_pnt.positions.at(iAx)-m_target_Q_now(iAx) ) >m_path_tolerance)
        m_is_in_path_tolerance=false;
    }

    m_is_in_tolerance=m_is_in_tolerance && (m_scaled_time-m_thor_prefilter->trjTime()).toSec()>0;
    m_mtx.unlock();

    std_msgs::BoolPtr msg(new std_msgs::Bool());;
    msg->data=m_is_in_tolerance && m_is_in_path_tolerance;
    m_tolerance_pub.publish(msg);

    std_msgs::Float64Ptr scaling_msg(new std_msgs::Float64());
    scaling_msg->data=m_scaling;
    m_scaling_pub.publish(scaling_msg);

    ROS_INFO("qui");
//    ROS_WARN_STREAM("pos = " << m_mpc_model->getState().segment(0,m_nax).transpose());
//    ROS_INFO_STREAM("vel = " << m_mpc_model->getState().segment(m_nax,m_nax).transpose());
//    ROS_ERROR_STREAM("vel_target = " << m_target_Dq.head(m_nax).transpose());
//    ROS_ERROR_STREAM("acc = " << m_next_acc.transpose());
//    ROS_ERROR("s_ref=%f \t scaling=%f\n",m_target_scaling,m_scaling);

    return true;
  }

  bool TaskBasedPosVelEffController::init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
  {
    m_ctrl.reset(new TaskBasedController());
    m_root_nh = root_nh;
    m_controller_nh = controller_nh;
    m_hw = hw;
    if (!m_ctrl->init(root_nh,controller_nh))
      return false;

    m_joint_names=m_ctrl->getJointNames();
    m_nAx=m_joint_names.size();
    for (unsigned int iDim = 0;iDim<m_nAx;iDim++)
    {
      try
      {
        m_handles.push_back(m_hw->getHandle(m_joint_names.at(iDim)));
      }
      catch (std::exception& ex)
      {
        ROS_ERROR("%s is not handled by %s. expection: %s",m_joint_names.at(iDim).c_str(),root_nh.getNamespace().c_str(),ex.what());
        return false;
      }
    }

    return true;
  }

  bool TaskBasedPosController::init(hardware_interface::PositionJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
  {
    m_ctrl.reset(new TaskBasedController());
    m_root_nh = root_nh;
    m_controller_nh = controller_nh;
    m_hw = hw;
    if (!m_ctrl->init(root_nh,controller_nh))
      return false;

    m_joint_names=m_ctrl->getJointNames();
    m_nAx=m_joint_names.size();
    for (unsigned int iDim = 0;iDim<m_nAx;iDim++)
    {
      try
      {
        m_handles.push_back(m_hw->getHandle(m_joint_names.at(iDim)));
      }
      catch (std::exception& ex)
      {
        ROS_ERROR("%s is not handled by %s. expection: %s",m_joint_names.at(iDim).c_str(),root_nh.getNamespace().c_str(),ex.what());
        return false;
      }
    }

    return true;
  }

}
